package domain;


import java.time.LocalDate;
import java.util.Objects;

public class Vehicle {

    private long id;
    private String brand;
    private String model;
    private LocalDate creationDate;
    private String color;
    private User owner;
    private String plateNumber;


    public Vehicle(long id, String brand, String model, LocalDate creationDate, String color, User owner,String plateNumber) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.creationDate = creationDate;
        this.color = color;
        this.owner = owner;
        this.plateNumber = plateNumber;
    }

    public Vehicle(long id, String brand, String model, LocalDate creationDate, String color, String plateNumber) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.creationDate = creationDate;
        this.color = color;
        this.plateNumber = plateNumber;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return id == vehicle.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", creationDate=" + creationDate +
                ", color='" + color + '\'' +
                ", plateNumber='" + plateNumber + '\'' +
                '}';
    }
}
