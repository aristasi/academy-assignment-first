package persistence;

import domain.Part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PartDAOImpl implements PartDAO {

    private static List<Part> parts = new ArrayList<>();

    @Override
    public Part insert(Part part){
        if (findById(part.getId()) != null) {
            System.out.println("Duplicate");
            return null;
        }
        parts.add(part);
        return part;
    }

    @Override
    public List<Part> findAll() {
        return parts;
    }

    @Override
    public Part findById(long id) {

        return parts.stream().filter(part -> part.getId() == id).
                findAny().
                orElse(null);

    }

    @Override
    public Part findByType(String type) {

        return parts.stream().filter(part -> part.getType().equals(type)).
                findAny().
                orElse(null);

    }

    @Override
    public boolean updateById(Part part) {
        if (findById(part.getId()) != null) {
            Part previousPart = findById(part.getId());
            updateFields(part, previousPart);
            return true;
        }
        return false;
    }

    private void updateFields(Part part, Part previousPart) {
        previousPart.setCost(part.getCost());
        previousPart.setName(part.getName());
        previousPart.setType(part.getType());
    }

    @Override
    public boolean delete(long id){

        return parts.removeIf(part -> part.getId() == id);
    }

}
