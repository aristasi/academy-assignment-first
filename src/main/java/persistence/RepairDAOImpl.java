package persistence;

import domain.Repair;
import domain.Vehicle;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class RepairDAOImpl implements RepairDAO {

    private static List<Repair> repairs = new ArrayList<>();

    @Override
    public Repair insert(Repair repair){
        if (findById(repair.getId()) != null) {
            System.out.println("Duplicate");
            return null;
        }
        repairs.add(repair);
        return repair;
    }

    @Override
    public List<Repair> findAll(){
        return repairs;
    }

    @Override
    public Repair findById(long id){

        return repairs.stream().filter(repair -> repair.getId() == id).
                findAny().
                orElse(null);

    }


    @Override
    public List<Repair> findByVehicle(Vehicle vehicle){

        return repairs.stream().filter(repair -> repair.getVehicle().equals(vehicle)).
                collect(Collectors.toList());
    }

    @Override
    public boolean delete(long id) {

        return repairs.removeIf(repair -> repair.getId() == id);
    }

    @Override
    public boolean updateById(Repair repair){
        if (findById(repair.getId()) != null) {
            Repair previousRepair = findById(repair.getId());
            updateFields(repair, previousRepair);
            return true;
        }
        return false;
    }

    private void updateFields(Repair repair, Repair previousRepair) {
        previousRepair.setAppointmentDate(repair.getAppointmentDate());
        previousRepair.setCost(repair.getCost());
        previousRepair.setStatus(repair.getStatus());
        previousRepair.setVehicle(repair.getVehicle());
        previousRepair.setParts(repair.getParts());
    }

    @Override
    public void calculateTotalCostOfARepair(long repairId){
        double sum = 0;
        for (Repair repair2: repairs) {
            if (repair2.getId() == repairId){
                for (int i = 0; i <repair2.getParts().size() ; i++) {
                     sum +=repair2.getParts().get(i).getCost();
                }
                double generalSum = sum + repair2.getCost();
                System.out.println("The total cost is: " + generalSum);
            }
        }
    }

}
