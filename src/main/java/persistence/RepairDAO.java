package persistence;

import domain.Repair;
import domain.Vehicle;

import java.util.List;

public interface RepairDAO {
    Repair insert(Repair repair);

    List<Repair> findAll();

    Repair findById(long id);

    List<Repair> findByVehicle(Vehicle vehicle);

    boolean delete(long id);

    boolean updateById(Repair repair);

    void calculateTotalCostOfARepair(long repairId);
}
