package persistence;

import domain.User;
import domain.Vehicle;

import java.util.List;

public interface UserDAO {
    User insert(User user);

    boolean delete(long userId);

    boolean updateById(User user);

    List<User> findAll();

    User findById(long id);

    User findByEmail(String email);

    boolean login(String email, String password);

    List<Vehicle> findAllVehiclesOfAUser(long id);
}
