package persistence;

import domain.User;
import domain.Vehicle;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class UserDAOImpl implements UserDAO {

    private static List<User> users = new ArrayList<>();


    @Override
    public User insert(User user) {
        if (findById(user.getId()) != null) {
            System.out.println("Duplicate");
            return null;
        }
        users.add(user);
        return user;
    }


    @Override
    public boolean delete(long userId) {

        return users.removeIf(user -> user.getId() == userId);
    }

    @Override
    public boolean updateById(User user) {
        if (findById(user.getId()) != null) {
            User previousUser = findById(user.getId());
            updateFields(user, previousUser);
//            int previousIndex = users.indexOf(previousUser);
//            users.set(previousIndex, user);
            return true;
        }
        return false;
    }

    private void updateFields(User user, User previousUser) {
        previousUser.setAddress(user.getAddress());
        previousUser.setEmail(user.getEmail());
        previousUser.setAfm(user.getAfm());
        previousUser.setFirstName(user.getFirstName());
        previousUser.setLastName(user.getLastName());
        previousUser.setType(user.getType());
        previousUser.setPassword(user.getPassword());
        previousUser.setVehicles(user.getVehicles());
    }

    @Override
    public List<User> findAll() {

        return users;
    }

    @Override
    public User findById(long id) {

        return users.stream().filter(user -> user.getId() == id).
                findAny().
                orElse(null);
    }


    @Override
    public User findByEmail(String email) {

        return users.stream().filter(user -> user.getEmail().equals(email)).
                findAny().
                orElse(null);
    }


    @Override
    public boolean login(String email, String password) {
        return users.stream()
                .anyMatch(user -> (user.getEmail().equals(email)) && (user.getPassword().equals(password)));
    }

    @Override
    public List<Vehicle> findAllVehiclesOfAUser(long id) {

        return users.stream().filter(user -> user.getId() == id).
                flatMap(user -> user.getVehicles().stream())
                .collect(Collectors.toList());
    }

}

//    public void addVehicleToUser(long id,Vehicle vehicle){
//        List<Vehicle> userVehicles = new ArrayList<>();
//        for (int i = 0; i <users.size()-1 ; i++) {
//            User user = findById(id);
//            user.addVehicleToList(vehicle);
//            updateById(id,user);
//            }
//        }

//    public boolean updateById(long userId, User user) {
//        boolean flag = false;
//        Iterator<User> userIterator = users.iterator();
//        while (userIterator.hasNext()) {
//            User UpdatedUser = userIterator.next();
//            if (UpdatedUser.getId() == userId) {
//                int previousIndex = users.indexOf(UpdatedUser);
//                users.set(previousIndex, user);
//                flag = true;
//            }
//        }
//        return flag;
//    }

//  public boolean delete(long userId){
//        boolean flag = false;
//        Iterator<User> userIterator = users.iterator();
//        while (userIterator.hasNext()) {
//            if (userIterator.next().getId() == userId) {
//                userIterator.remove();
//                 flag = true;
//            }
//        }
//        return flag;
//    }