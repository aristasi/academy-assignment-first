package persistence;

import domain.Repair;
import domain.Vehicle;
import service.RepairService;
import service.RepairServiceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VehicleDAOImpl implements VehicleDAO {

    private static List<Vehicle> vehicles = new ArrayList<>();
    RepairService repairService = new RepairServiceImpl();


    @Override
    public Vehicle insert(Vehicle vehicle){
        if (findById(vehicle.getId()) != null) {
            System.out.println("Duplicate");
            return null;
        }
        vehicles.add(vehicle);
        return vehicle;
    }

    @Override
    public List<Vehicle> findAll(){
        return vehicles;
    }

    @Override
    public Vehicle findById(long id){
        return vehicles.stream().filter(vehicle -> vehicle.getId() == id)
                .findAny()
                .orElse(null);

    }

    @Override
    public Vehicle findByPlateNumber(String plateNumber){

        return vehicles.stream().filter(vehicle -> vehicle.getPlateNumber().equals(plateNumber))
                .findAny()
                .orElse(null);
    }

    @Override
    public boolean delete(long id){

        return vehicles.removeIf(vehicle -> vehicle.getId() == id);
    }

    @Override
    public boolean updateById(Vehicle vehicle){
        if (findById(vehicle.getId()) != null) {
            Vehicle previousVehicle = findById(vehicle.getId());
            updateFields(vehicle, previousVehicle);
            return true;
        }
        return false;
    }

    private void updateFields(Vehicle vehicle, Vehicle previousVehicle) {
        previousVehicle.setBrand(vehicle.getBrand());
        previousVehicle.setColor(vehicle.getColor());
        previousVehicle.setCreationDate(vehicle.getCreationDate());
        previousVehicle.setModel(vehicle.getModel());
        previousVehicle.setOwner(vehicle.getOwner());
        previousVehicle.setPlateNumber(vehicle.getPlateNumber());
    }

}
