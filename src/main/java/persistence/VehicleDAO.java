package persistence;

import domain.Repair;
import domain.Vehicle;

import java.util.List;

public interface VehicleDAO {
    Vehicle insert(Vehicle vehicle);

    List<Vehicle> findAll();

    Vehicle findById(long id);

    Vehicle findByPlateNumber(String plateNumber);

    boolean delete(long id);

    boolean updateById(Vehicle vehicle);

}
