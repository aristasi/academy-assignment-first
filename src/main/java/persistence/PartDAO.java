package persistence;

import domain.Part;

import java.util.List;

public interface PartDAO {
    Part insert(Part part);

    List<Part> findAll();

    Part findById(long id);

    Part findByType(String type);

    boolean updateById(Part part);

    boolean delete(long id);
}
