package service;

import domain.Repair;
import domain.Vehicle;

import java.time.LocalDate;
import java.util.List;

public interface VehicleService {
    Vehicle insert(long id, String brand, String model, LocalDate creationDate, String color, String plateNumber);

    Vehicle insert(Vehicle vehicle);

    List<Vehicle> findAll();

    Vehicle findById(long id);

    Vehicle findByPlateNumber(String plateNumber);

    boolean delete(long id);

    boolean updateById(Vehicle vehicle);

    List<Repair> findAllRepairs(long id);
}
