package service;

import domain.Repair;
import domain.User;
import domain.Vehicle;
import persistence.UserDAO;
import persistence.UserDAOImpl;
import utilities.UserType;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    UserDAO userDAO = new UserDAOImpl();
    RepairService repairService = new RepairServiceImpl();



    @Override
    public User insert(long id, String email, String password, String firstName, String lastName, String address, int afm, UserType type) {
            User user = new User(id, email, password, firstName, lastName, address, afm, type);
            return userDAO.insert(user);
    }


    @Override
    public User insert(User user) {
        return userDAO.insert(user);
    }

    @Override
    public boolean delete(long id){
        return userDAO.delete(id);
    }

    @Override
    public boolean updateById(User user) {
        return userDAO.updateById(user);
    }

    @Override
    public List<User> findAll(){
        return userDAO.findAll();
    }

    @Override
    public User findById(long id) {
        return userDAO.findById(id);
    }

    @Override
    public User findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public boolean login(String email, String password){

        return userDAO.login(email, password);
    }

    @Override
    public List<Vehicle> findAllVehiclesOfAUser(long userId){
        return userDAO.findAllVehiclesOfAUser(userId);
    }

    @Override
    public List<Repair> findAllRepairsOfAllVehiclesOfASpecificUser(long userid){
        List<Vehicle> vehicles = userDAO.findAllVehiclesOfAUser(userid);
        List<Repair> listOfRepairs = new ArrayList<>();
        for (Vehicle vehicle : vehicles) {
             listOfRepairs.addAll(repairService.findByVehicle(vehicle));
        }
        return listOfRepairs;
    }
}
