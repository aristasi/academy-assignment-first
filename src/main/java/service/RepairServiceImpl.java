package service;

import domain.Part;
import domain.Repair;
import domain.Vehicle;
import persistence.RepairDAO;
import persistence.RepairDAOImpl;

import java.time.LocalDate;
import java.util.List;

public class RepairServiceImpl implements RepairService {

    RepairDAO repairDAO = new RepairDAOImpl();

    @Override
    public Repair insert(long id, LocalDate date, String status, double cost, Vehicle vehicle, List<Part> parts){
        Repair repair = new Repair(id,date,status,cost,vehicle,parts);
        return repairDAO.insert(repair);
    }

    @Override
    public Repair insert(Repair repair) {
       return repairDAO.insert(repair);
    }

    @Override
    public List<Repair> findAll() {

        return repairDAO.findAll();
    }

    @Override
    public Repair findById(long id) {

        return repairDAO.findById(id);
    }

    @Override
    public List<Repair> findByVehicle(Vehicle vehicle){
        return repairDAO.findByVehicle(vehicle);
    }

    @Override
    public boolean delete(long id) {

        return repairDAO.delete(id);
    }

    @Override
    public boolean updateById(Repair repair){

        return repairDAO.updateById(repair);
    }

    @Override
    public void calculateTotalCostOfARepair(long repairId) {

       repairDAO.calculateTotalCostOfARepair(repairId);
    }

}

