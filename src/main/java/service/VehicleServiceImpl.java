package service;

import domain.Repair;
import domain.Vehicle;
import persistence.VehicleDAO;
import persistence.VehicleDAOImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VehicleServiceImpl implements VehicleService {

    VehicleDAO vehicleDAO = new VehicleDAOImpl();
    RepairService repairService = new RepairServiceImpl();

    @Override
    public Vehicle insert(long id, String brand, String model, LocalDate creationDate, String color, String plateNumber) {
        Vehicle vehicle = new Vehicle(id, brand, model, creationDate, color, plateNumber);
       return vehicleDAO.insert(vehicle);
    }

    @Override
    public Vehicle insert(Vehicle vehicle) {
       return vehicleDAO.insert(vehicle);
    }

    @Override
    public List<Vehicle> findAll() {
        return vehicleDAO.findAll();
    }

    @Override
    public Vehicle findById(long id) {
        return vehicleDAO.findById(id);
    }

    @Override
    public Vehicle findByPlateNumber(String plateNumber) {
        return vehicleDAO.findByPlateNumber(plateNumber);
    }

    @Override
    public boolean delete(long id) {
        return vehicleDAO.delete(id);
    }

    @Override
    public boolean updateById(Vehicle vehicle) {
        return vehicleDAO.updateById(vehicle);
    }

    @Override
    public List<Repair> findAllRepairs(long id) {
        List<Repair> allRepairsOfAVehicle = new ArrayList<>();
        for (Vehicle vehicle: findAll()) {
            if (vehicle.getId() == id) {
                return repairService.findByVehicle(vehicle);
            }
        }
        return allRepairsOfAVehicle;
//        return findAll().stream().filter(vehicle -> vehicle.getId() == id).forEach(vehicle -> repairService.findById(vehicle.getId()));
    }

}
