package service;

import domain.Part;
import persistence.PartDAO;
import persistence.PartDAOImpl;

import java.util.List;

public class PartServiceImpl implements PartService {

    PartDAO partDAO = new PartDAOImpl();

    @Override
    public Part insert(long id, String name, String type, double cost){
        Part part = new Part(id, name, type, cost);
        return partDAO.insert(part);
    }

    @Override
    public Part insert(Part part){
        return partDAO.insert(part);
    }

    @Override
    public List<Part> findAll(){

        return partDAO.findAll();
    }

    @Override
    public Part findById(long id){

        return partDAO.findById(id);
    }

    @Override
    public Part findByType(String type){

        return partDAO.findByType(type);
    }

    @Override
    public boolean updateById(Part part){

        return partDAO.updateById(part);
    }

    @Override
    public boolean delete(long id){

        return partDAO.delete(id);
    }

}
