package service;

import domain.Repair;
import domain.User;
import domain.Vehicle;
import utilities.UserType;

import java.util.List;

public interface UserService {
    User insert(long id, String email, String password, String firstName, String lastName, String address, int afm, UserType type);

    User insert(User user);

    boolean delete(long id);

    boolean updateById(User user);

    List<User> findAll();

    User findById(long id);

    User findByEmail(String email);

    boolean login(String email, String password);

    List<Vehicle> findAllVehiclesOfAUser(long userId);

    List<Repair> findAllRepairsOfAllVehiclesOfASpecificUser(long userid);
}
