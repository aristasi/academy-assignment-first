package service;

import domain.Part;

import java.util.List;

public interface PartService {
    Part insert(long id, String name, String type, double cost);

    Part insert(Part part);

    List<Part> findAll();

    Part findById(long id);

    Part findByType(String type);

    boolean updateById(Part part);

    boolean delete(long id);
}
