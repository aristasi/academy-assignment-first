package service;

import domain.Part;
import domain.Repair;
import domain.Vehicle;

import java.time.LocalDate;
import java.util.List;

public interface RepairService {
    Repair insert(long id, LocalDate date, String status, double cost, Vehicle vehicle, List<Part> parts);

    Repair insert(Repair repair);

    List<Repair> findAll();

    Repair findById(long id);

    List<Repair> findByVehicle(Vehicle vehicle);

    boolean delete(long id);

    boolean updateById(Repair repair);

    void calculateTotalCostOfARepair(long repairId);


}
