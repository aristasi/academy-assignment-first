import controller.PartController;
import controller.RepairController;
import controller.UserController;
import controller.VehicleController;
import domain.Part;
import domain.Repair;
import domain.User;
import domain.Vehicle;
import service.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static utilities.UserType.ADMIN;
import static utilities.UserType.USER;

public class Main {

    UserController userController = new UserController();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("----------Vehicle Repair Shop---------- \n");
        String lastOption = "";
        while (!lastOption.equalsIgnoreCase("x")) {
            lastOption = displayMenu(scanner);
        }
        System.out.println("\nExiting System...\n");
    }

    public static String displayMenu(Scanner scanner) {
        UserController userController = new UserController();
        VehicleController vehicleController = new VehicleController();
        RepairController repairController = new RepairController();
        PartController partController = new PartController();

        System.out.println("Please select a menu to navigate:\n");
        System.out.println("1) User");
        System.out.println("2) Vehicle");
        System.out.println("3) Repair");
        System.out.println("4) Part");
        System.out.println("X.  Exit System.");
        System.out.print("Option: ");
        String option = scanner.next();
        switch (option) {
            case "1": userController.userDisplayMenu();
                return option;
            case "2": vehicleController.vehicleDisplayMenu();
                return option;
            case "3": repairController.repairDisplayMenu();
                return option;
            case "4": partController.partDisplayMenu();
                return option;
            case "x":
                return option;
            default: System.out.println("Invalid option, please re-enter.");
                return option;
        }
    }

}


//        UserService userService = new UserServiceImpl();
//        RepairService repairService = new RepairServiceImpl();
//        VehicleService vehicleService = new VehicleServiceImpl();
//        PartService partService = new PartServiceImpl();


//        //USERS
//
//        User user = new User(1, "aris@aris.gr", "newpass", "Aris", "Tasios", "Triantafyllidi 33", 133527469, ADMIN);
//        User user2 = new User(2, "dimitris@dimitris.gr", "pass", "Dimitris", "Tasios", "Thermopilon 52", 133527460, USER);
//        User user3 = new User(3, "christos@dimitris.gr", "password", "Christos", "Tasios", "Thiatiron 82", 127333353, USER);
//        User user4 = userService.insert(4, "george@george.gr", "georgepassword", "George", "Tasios", "Chatzistavrou", 133527458, USER);
//        User userFive = new User(300,"updated@user.gr","www","Ion","Bion","adee",133578464, ADMIN);
//
//
//        //DATES OF CREATION
//
//        LocalDate newd1 = LocalDate.of(2015, 1, 1);
//        LocalDate newd2 = LocalDate.of(2016, 2, 2);
//        LocalDate newd3 = LocalDate.of(2017, 3, 3);
//        LocalDate newd4 = LocalDate.of(2010, 5, 3);
//        LocalDate newd5 = LocalDate.of(2014, 8, 10);
//        LocalDate newd6 = LocalDate.of(2018, 1, 2);
//
//
//        //DATES OF REPAIRS
//
//        LocalDate newRepairDate7 = LocalDate.of(2018, 10, 2);
//        LocalDate newRepairDate8 = LocalDate.of(2019, 2, 1);
//        LocalDate newRepairDate9 = LocalDate.of(2018, 5, 8);
//        LocalDate newRepairDate10 = LocalDate.of(2019, 3, 5);
//        LocalDate newRepairDate11 = LocalDate.of(2018, 9, 6);
//        LocalDate newRepairDate12 = LocalDate.of(2019, 1, 3);
//        LocalDate newRepairDate13 = LocalDate.of(2019, 2, 2);
//
//
//        //VEHICLES
//
//        Vehicle vehicleOne = new Vehicle(10, "Toyota", "Yaris", newd1, "Blue", user, "AHK7055");
//        Vehicle vehicleTwo = new Vehicle(20, "VW", "Polo", newd2, "Yellow", user2, "AHK7058");
//        Vehicle vehicleThree = new Vehicle(30, "BMW", "116", newd3, "Yellow", user3, "AHE2002");
//        Vehicle vehicleFour = new Vehicle(40, "MB", "AClass", newd4, "Black", user, "AHK6651");
//        Vehicle vehicleFive = new Vehicle(50, "Opel", "Astra", newd5, "Red", user2, "AHE5325");
//        Vehicle vehicleSix = new Vehicle(60, "AUDI", "A3", newd6, "White", user3, "AHK2000");
//        Vehicle vehicleSeven = new Vehicle(70, "Renault", "A3", newd6, "White", user4, "AHM5462");
//        Vehicle vehicleEight = vehicleService.insert(80, "Pegeuot", "207", newd1, "Black","AHH5325");
//        Vehicle vehicleNine = vehicleService.insert(90, "Pegeuot", "307", newd1, "Black", "AHK7059");
//
//        //PARTS
//
//        Part engine = new Part(100, "Engine", "EngineType", 500.0);
//        Part wheel = new Part(200, "Wheel", "WheelType", 100.0);
//        Part brakes = new Part(300, "Brakes", "BrakeType", 150.0);
//        Part gearBox = new Part(400, "GearBox", "GearboxType", 220.0);
//        Part oilFilter = new Part(500, "OilFilter", "Filter", 10.0);
//        Part airFilter = new Part(600, "airFilter", "Filter", 20.0);
//        Part tyre = partService.insert(700, "Tyre", "TyreType", 70.0);
//
//        //REPAIRS
//
//        Repair repairOne = new Repair(11, newRepairDate7, "ServiceOne", 150.0, vehicleOne);
//        Repair repairTwo = new Repair(22, newRepairDate8, "ServiceTwo", 100.0, vehicleTwo);
//        Repair repairThree = new Repair(33, newRepairDate9, "ServiceThree", 200.0, vehicleThree);
//        Repair repairFour = new Repair(44, newRepairDate10, "ServiceFour", 250.0, vehicleFour);
//        Repair repairFive = new Repair(55, newRepairDate11, "ServiceFive", 100.0, vehicleFive);
//        Repair repairSix = new Repair(66, newRepairDate12, "ServiceSix", 300.0, vehicleSix);
//        Repair repairSeven = new Repair(77, newRepairDate13, "ServiceSeven", 120.0, vehicleOne);
//        Repair repairEight = new Repair(88, newRepairDate13, "ServiceEight", 111.0, vehicleEight);
//        Repair repairNine = repairService.insert(99, newRepairDate7, "ServiceNine", 500.0, vehicleOne, new ArrayList<>());
//
//
//        // ADD USERS TO LIST
//        userService.insert(user);
//        userService.insert(user2);
//        userService.insert(user3);
//
//
//        //ADD VEHICLES TO USER
//        user.addVehicleToList(vehicleOne);
//        user.addVehicleToList(vehicleTwo);
//        user2.addVehicleToList(vehicleThree);
//        user2.addVehicleToList(vehicleFour);
//        user3.addVehicleToList(vehicleFive);
//        user3.addVehicleToList(vehicleSix);
//        user4.addVehicleToList(vehicleSeven);
//        user4.addVehicleToList(vehicleEight);
//        user.addVehicleToList(vehicleNine);
//
//
//        // ADD PARTS TO REPAIR
//        repairOne.addPartToList(engine);
//        repairOne.addPartToList(wheel);
//        repairTwo.addPartToList(brakes);
//        repairThree.addPartToList(gearBox);
//        repairFour.addPartToList(oilFilter);
//        repairFive.addPartToList(airFilter);
//        repairSix.addPartToList(engine);
//        repairSeven.addPartToList(wheel);
//        repairSeven.addPartToList(oilFilter);
//        repairNine.addPartToList(oilFilter);
//        repairNine.addPartToList(tyre);
//
//        //ADD REPAIRS TO LIST
//        repairService.insert(repairOne);
//        repairService.insert(repairTwo);
//        repairService.insert(repairThree);
//        repairService.insert(repairFour);
//        repairService.insert(repairFive);
//        repairService.insert(repairSix);
//        repairService.insert(repairSeven);
//        repairService.insert(repairEight);
//
//
//        //VEHICLE DAO LIST
//        vehicleService.insert(vehicleOne);
//        vehicleService.insert(vehicleTwo);
//        vehicleService.insert(vehicleThree);
//        vehicleService.insert(vehicleFour);
//        vehicleService.insert(vehicleFive);
//        vehicleService.insert(vehicleSix);
//        vehicleService.insert(vehicleSeven);
//
//
//        //ADD PARTS TO DAO
//        partService.insert(engine);
//        partService.insert(wheel);
//        partService.insert(brakes);
//        partService.insert(gearBox);
//        partService.insert(oilFilter);
//        partService.insert(airFilter);
//
//
//
//
//        // USERS FUNCTIONALITY TESTING
//        List<User> listOfUsers = userService.findAll();
//        for (User us : listOfUsers) {
//            System.out.println(us);
//        }
////        User userTest1 = userService.findByEmail("aris@aris.gr");
////        System.out.println(userTest1);
////        boolean userUpd =userService.updateById(userFive);
////        System.out.println(userUpd);
////        List<User> newListOfUsers = userService.findAll();
////        for (User us : newListOfUsers) {
////            System.out.println(us);
////        }
////        userService.findAll();
////        boolean flag =userService.delete(3);
////        System.out.println(flag);
////
////        for (User us :
////                listOfUsers) {
////            System.out.println(us);
////
////        }
////
////        User userk = userService.findById(1);
////        System.out.println(userk);
////        User userTest = userService.findByEmail("dimitris@dimitris.gr");
////        System.out.println(userTest);
//        List<Vehicle> vehiclesOfAUser = userService.findAllVehiclesOfAUser(1);
//        for (Vehicle vehicle :
//                vehiclesOfAUser) {
//            System.out.println(vehicle);
//        }
////        System.out.println(repairNine);
////        List<Repair> repairsOfAUser = userService.findAllRepairsOfAllVehiclesOfASpecificUser(1);
////        for (Repair repair :
////                repairsOfAUser) {
////            System.out.println(repair);
////        }
//        boolean b5 = userService.login("aris@aris.gr","newpass");
//        System.out.println(b5);
////        boolean b6 = userService.login("aris@s.gr","newpass");
////        System.out.println(b6);
////
////        List<Vehicle> userTest2 = userService.findAllVehiclesOfAUser(1);
////        for (Vehicle vehicle :
////                userTest2) {
////            System.out.println(vehicle);
////        }
////        List<Repair> rep = userService.findAllRepairsOfAllVehiclesOfASpecificUser(1);
////        for (Repair reps :
////                rep) {
////            System.out.println(reps);
////        }
//
//        // VEHICLE FUNCTIONALITY TESTING
////        List<Vehicle> listOfVehicles = vehicleService.findAll();
////        for (Vehicle vehicle :listOfVehicles) {
////            System.out.println(vehicle);
////        }
//        Vehicle testVehicleId = vehicleService.findById(10);
//        System.out.println(testVehicleId);
////        boolean b12 = vehicleService.delete(70);
////        System.out.println(b12);
////        List<Vehicle> listOfVehicles = vehicleService.findAll();
////        for (Vehicle vehicle :listOfVehicles) {
////            System.out.println(vehicle);
////        }
//
////        boolean b =vehicleService.updateById(10, vehicleEight);
////        System.out.println(b);
////          boolean b17 = vehicleService.delete(90);
////          System.out.println(b17);
////        Vehicle testVehicle = vehicleService.findByPlateNumber("AHK7058");
////        System.out.println(testVehicle);
//        List<Repair> listOfRepairs = vehicleService.findAllRepairs(10);
//        System.out.println(listOfRepairs);
//
//        // REPAIR FUNCTIONALITY TESTING
//        //boolean bb= repairService.updateById(11, repairEight);
//        //System.out.println(bb);
////       List<Repair> NewList = repairService.findAll();
////        for (Repair repair :
////                NewList) {
////            System.out.println(repair);
////        }
////        List<Repair> newlist1 = repairService.findByVehicle(vehicleOne);
////        for (Repair repair :
////                newlist1) {
////            System.out.println(repair);
////        }
////        Repair rep22 =repairService.findById(77);
////        System.out.println(rep22);
//        repairService.calculateTotalCostOfARepair(77);
//
//            // PART FUNCTIONALITY TESTING
//            //boolean p =partService.updateById(100, tyre);
//            //System.out.println(p);
//        List<Part> listOfParts =partService.findAll();
//        for (Part part :
//                listOfParts) {
//            System.out.println(part);
//
//        }
//        Part search = partService.findById(200);
//        System.out.println(search);
//        Part searchType =partService.findByType("EngineType");
//        System.out.println(searchType);
//        boolean b175 =partService.delete(520);
//        System.out.println(b175);
//


