package controller;

import domain.Part;
import domain.Repair;
import domain.Vehicle;
import service.RepairService;
import service.RepairServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RepairController {

    RepairService repairService = new RepairServiceImpl();
    PartController partController = new PartController();
    VehicleController vehicleController = new VehicleController();

    public String repairDisplayMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("------------Repair Menu------------\n");
        System.out.println("Please select a menu to navigate:\n");
        System.out.println("1) Add");
        System.out.println("2) Delete ");
        System.out.println("3) Find All ");
        System.out.println("4) Find by id ");
        System.out.println("5) Total cost of a repair ");
        System.out.println("enter 0 to return to main menu.");
        System.out.print("Option: ");
        String option = scanner.next();
        switch (option) {
            case "1": createRepair();
                return option;
            case "2": deleteRepair();
                return option;
            case "3": findAllRepairs();
                return option;
            case "4": findRepairById();
                return option;
            case "5": calculateTotalCost();
                return option;
            case "0":
                return option;
            default: System.out.println("\nInvalid option, please re-enter.\n");
                return repairDisplayMenu();
        }
    }

    public void createRepair (){
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give data for the repair:\n");
        System.out.println(" Give an ID: ");
        String repairId = scanner.nextLine();
        long id = Long.valueOf(repairId);
        System.out.println(" Status ");
        String status = scanner.nextLine();
        System.out.println(" Cost: ");
        String repairCost = scanner.nextLine();
        double cost = Double.valueOf(repairCost);
        System.out.println("Vehicle ID: ");
        String vehId = scanner.nextLine();
        long vehicleId = Long.valueOf(vehId);
        Vehicle vehicle = vehicleController.findById(vehicleId);
        System.out.println("Year of Repair: ");
        String year = scanner.nextLine();
        int yearOfRep = Integer.valueOf(year);
        System.out.println("Month of Repair(two digits: ex 08): ");
        String month = scanner.nextLine();
        int monthOfRep = Integer.valueOf(month);
        System.out.println("Day of Repair(two digits: ex 08): ");
        String day = scanner.nextLine();
        int dayOfRep = Integer.valueOf(day);
        LocalDate dateOfRepair = LocalDate.of(yearOfRep,monthOfRep,dayOfRep);
        System.out.println("Number of parts: ");
        String number = scanner.nextLine();
        int numberOfParts = Integer.valueOf(year);
        List<Part> partsOfRepair= new ArrayList<>();
        for (int i = 0; i <numberOfParts; i++) {
            System.out.println("Part Id: ");
            String part = scanner.nextLine();
            long partId = Long.valueOf(part);
            partsOfRepair.add(partController.findById(partId));
        }
        insert(id,dateOfRepair,status,cost,vehicle,partsOfRepair);
        System.out.println("User entered successfully.\n\n");
        System.out.println(findById(id));
    }

    public void deleteRepair () {
        Scanner scanner = new Scanner(System.in);
        List<Repair> listOfRepairs = findAll();
        System.out.println(listOfRepairs);
        System.out.println("\n\nPlease give the Repair-Id that you want to delete:\n");
        String repairId = scanner.nextLine();
        long id = Long.valueOf(repairId);
        delete(id);
        System.out.println("Repair deleted successfully.\n\n");
        System.out.println("The new list of repairs is: ");
        System.out.println(findAll());
    }

    public void findAllRepairs () {
        System.out.println("\n\nList Of Repairs :\n");
        List<Repair> listOfRepairs = findAll();
        if (listOfRepairs.isEmpty()){
            System.out.println("There aren't any Repairs");
        }else {
            listOfRepairs.stream().
                    forEach(System.out::println);
            System.out.println("");
        }
    }

    public void findRepairById () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give the Repair-Id:\n");
        String repairId = scanner.nextLine();
        long convertedId = Long.valueOf(repairId);
        List<Repair> listOfRepairs = findAll();
        List<Long> existingIds = new ArrayList<>();
        for (Repair repair: listOfRepairs) {
            long id = repair.getId();
            existingIds.add(id);
        }
        if(existingIds.contains(convertedId)){
            Repair result = findById(convertedId);
            System.out.println("Repair found successfully.\n\n");
            System.out.println(result);
        } else {
            System.out.println("This Id does not exist, please enter an other Id");
            findRepairById();
        }
    }

    public void calculateTotalCost(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give the Repair-Id:\n");
        String repairId = scanner.nextLine();
        Long convertedId = Long.valueOf(repairId);
        calculateTotalCostOfARepair(convertedId);
    }



    public Repair insert(long id, LocalDate date, String status, double cost, Vehicle vehicle, List<Part> parts){
        Repair repair = new Repair(id,date,status,cost,vehicle,parts);
        return repairService.insert(repair);
    }

    public List<Repair> findAll() {

        return repairService.findAll();
    }

    public Repair findById(long id) {

        return repairService.findById(id);
    }

    public boolean delete(long id) {

        return repairService.delete(id);
    }

    public boolean updateById(Repair repair){

        return repairService.updateById(repair);
    }

    public void calculateTotalCostOfARepair(long repairId) {

        repairService.calculateTotalCostOfARepair(repairId);
    }

}
