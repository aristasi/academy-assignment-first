package controller;

import domain.Repair;
import domain.User;
import domain.Vehicle;
import service.VehicleService;
import service.VehicleServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VehicleController {

    VehicleService vehicleService = new VehicleServiceImpl();
    UserController userController = new UserController();

    public String vehicleDisplayMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("------------Vehicle Menu------------\n");
        System.out.println("Please select a menu to navigate:\n");
        System.out.println("1) Add");
        System.out.println("2) Delete ");
        System.out.println("3) Find All ");
        System.out.println("4) Find by id ");
        System.out.println("5) Find by plate number ");
        System.out.println("6) Find all repairs of a vehicle");
        System.out.println("enter 0 to return to main menu.");
        System.out.print("Option: ");
        String option = scanner.next();
        switch (option) {
            case "1": createVehicle();
                return option;
            case "2": deleteVehicle();
                return option;
            case "3": findAllVehicles();
                return option;
            case "4": findVehicleById();
                return option;
            case "5": findVehicleByPlateNumber();
                return option;
            case "6": findAllRepairsOfAVehicle();
                return option;
            case "0":
                return option;
            default: System.out.println("\nInvalid option, please re-enter.\n");
                return vehicleDisplayMenu();
        }
    }

    public void createVehicle(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give data for the vehicle:\n");
        System.out.println(" Give a vehicle ID: ");
        String vehicleId = scanner.nextLine();
        long id = Long.valueOf(vehicleId);
        System.out.println(" Brand: ");
        String brand = scanner.nextLine();
        System.out.println(" Model: ");
        String model = scanner.nextLine();
        System.out.println(" Color ");
        String color = scanner.nextLine();
        System.out.println("Plate Number ");
        String plateNumber = scanner.nextLine();
        System.out.println("Year of Construction: ");
        String year = scanner.nextLine();
        int yearOfCon = Integer.valueOf(year);
        System.out.println("Month of Construction(two digits: ex 08): ");
        String month = scanner.nextLine();
        int monthOfCon = Integer.valueOf(month);
        System.out.println("Day of Construction(two digits: ex 08): ");
        String day = scanner.nextLine();
        int dayOfCon = Integer.valueOf(day);
        LocalDate dateOfConstruction = LocalDate.of(yearOfCon,monthOfCon,dayOfCon);
        insert(id,brand,model,dateOfConstruction,color,plateNumber);
        System.out.println("Vehicle entered successfully.\n\n");
        System.out.println(findById(id));
        addNewVehicleToUser(scanner, vehicleId, id);

    }

    private void addNewVehicleToUser(Scanner scanner, String vehicleId, long id) {
        Vehicle vehicle = findById(id);
        System.out.println("\nAdd Vehicle to a User\n");
        System.out.println("Give a User Id.");
        String userId = scanner.nextLine();
        long userIdToAddVehicle = Long.valueOf(userId);
        User previousUser = userController.findById(userIdToAddVehicle);
        List<Vehicle> newList = previousUser.getVehicles();
        newList.add(vehicle);
        previousUser.setVehicles(newList);
        System.out.println("This User has:");
        userController.findAllVehiclesOfAUser(userIdToAddVehicle).forEach(System.out::println);

    }
    public void deleteVehicle () {
        Scanner scanner = new Scanner(System.in);
        List<Vehicle> listOfVehicles = findAll();
        System.out.println(listOfVehicles);
        System.out.println("\n\nPlease give the Vehicle-Id that you want to delete:\n");
        String vehicleId = scanner.nextLine();
        long id = Long.valueOf(vehicleId);
        delete(id);
        Vehicle vehicle = findById(id);
        System.out.println("Vehicle deleted successfully.\n\n");
        System.out.println("The new list of vehicles is: ");
        System.out.println(findAll());
    }

    public void findAllVehicles () {
        System.out.println("\n\nList Of Vehicles :\n");
        List<Vehicle> listOfVehicles = findAll();
        if (listOfVehicles.isEmpty()){
            System.out.println("There aren't any Vehicles");
        }else {
            listOfVehicles.stream().
                    forEach(System.out::println);
            System.out.println("");
        }
    }


    public void findVehicleById () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give the Vehicle-Id:\n");
        String vehicleId = scanner.nextLine();
        long convertedId = Long.valueOf(vehicleId);
        List<Vehicle> listOfVehicles = findAll();
        List<Long> existingIds = new ArrayList<>();
        for (Vehicle vehicle: listOfVehicles) {
            long id = vehicle.getId();
            existingIds.add(id);
        }
        if(existingIds.contains(convertedId)){
            Vehicle result = findById(convertedId);
            System.out.println("Vehicle found successfully.\n\n");
            System.out.println(result);
        } else {
            System.out.println("This Id does not exist, please enter an other Id");
            findVehicleById();
        }
    }

    public void findVehicleByPlateNumber () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give a Plate Number:\n");
        String plateNumber = scanner.nextLine();
        List<Vehicle> listOfVehicles = findAll();
        List<String> existingPlateNumbers = new ArrayList<>();
        for (Vehicle vehicle: listOfVehicles) {
            String email = vehicle.getPlateNumber();
            existingPlateNumbers.add(email);
        }
        if(existingPlateNumbers.contains(plateNumber)){
            Vehicle result = findByPlateNumber(plateNumber);
            System.out.println("Vehicle found successfully.\n\n");
            System.out.println(result);
        } else {
            System.out.println("This Plate Number does not exist, please enter an other plate Number");
            findVehicleByPlateNumber();
        }
    }

    public void findAllRepairsOfAVehicle () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\n Please give the Vehicle-Id :\n");
        String vehicleId = scanner.nextLine();
        long convertedId = Long.valueOf(vehicleId);
        List<Vehicle> listOfVehicles = findAll();
        List<Long> existingIds = new ArrayList<>();
        for (Vehicle vehicle : listOfVehicles) {
            long id = vehicle.getId();
            existingIds.add(id);
        }
        if (existingIds.contains(convertedId)) {
            if (findAllRepairs(convertedId).isEmpty()){
                System.out.println("There are no repairs for this vehicle");
            } else{
                System.out.println("Vehicle repairs: \n\n");
                findAllRepairs(convertedId).forEach(System.out::println);
            }
        } else {
            System.out.println("This Id does not exist, please enter an other Id");
            findAllRepairsOfAVehicle();
        }
    }



    public Vehicle insert(long id, String brand, String model, LocalDate creationDate, String color, String plateNumber) {
        return vehicleService.insert(id, brand, model, creationDate, color, plateNumber);
    }

    public List<Vehicle> findAll() {
        return vehicleService.findAll();
    }

    public Vehicle findById(long id) {
        return vehicleService.findById(id);
    }

    public Vehicle findByPlateNumber(String plateNumber) {
        return vehicleService.findByPlateNumber(plateNumber);
    }

    public boolean delete(long id) {
        return vehicleService.delete(id);
    }

    public boolean updateById(Vehicle vehicle) {
        return vehicleService.updateById(vehicle);
    }

    public List<Repair> findAllRepairs(long id) {
        return vehicleService.findAllRepairs(id);
    }

}
