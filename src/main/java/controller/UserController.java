package controller;

import com.sun.tools.javac.Main;
import domain.Repair;
import domain.User;
import domain.Vehicle;
import service.UserService;
import service.UserServiceImpl;
import utilities.UserType;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UserController {

    UserService userService = new UserServiceImpl();

    public String userDisplayMenu() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("------------User Menu------------\n");
        System.out.println("Please select a menu to navigate:\n");
        System.out.println("1) Add");
        System.out.println("2) Delete ");
        System.out.println("3) Find All ");
        System.out.println("4) Find by id ");
        System.out.println("5) Find by email ");
        System.out.println("6) Find all vehicles of a user ");
        System.out.println("7) Find all repairs of a vehicle of a user ");
        System.out.println("enter 0 to return to main menu.");
        System.out.print("Option: ");
        String option = scanner.next();
        switch (option) {
            case "1": createUser();
                return option;
            case "2": deleteUser();
                return option;
            case "3": findAllUsers();
                return option;
            case "4": findUserById();
                return option;
            case "5": findUserByEmail();
                return option;
            case "6": findAllVehiclesOfAUser();
                return option;
            case "7": findAllRepairsOfAVehicleOfASpecificUser();
                return option;
            case "0":
                return option;
            default: System.out.println("\nInvalid option, please re-enter.\n");
                return userDisplayMenu();
        }
    }

    public void createUser (){
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("\n\nPlease give data for the user:\n");
            System.out.println(" Give an ID: ");
            long id = Long.valueOf(scanner.nextLine());
            System.out.println(" First Name: ");
            String firstName = scanner.nextLine();
            System.out.println(" Last Name: ");
            String lastName = scanner.nextLine();
            System.out.println(" Address: ");
            String address = scanner.nextLine();
            System.out.println("Give an Email: ");
            String email = scanner.nextLine();
            System.out.println("Password: ");
            String pass = scanner.nextLine();
            System.out.println("AFM: ");
            int afm = Integer.valueOf(scanner.nextLine());
            System.out.println("USER or ADMIN ");
            String userTypeString = scanner.nextLine().toUpperCase();
            UserType userType = UserType.valueOf(userTypeString);
            insert(id, email, pass, firstName, lastName, address, afm, userType);
            System.out.println("User entered successfully.\n\n");
            System.out.println(findById(id));
        } catch (NumberFormatException e) {
            System.out.println("Please enter only digits is ID and AFM");
            createUser();
        }
    }


    public void deleteUser () {
        Scanner scanner = new Scanner(System.in);
        List<User> listOfUsers = findAll();
        System.out.println(listOfUsers);
        System.out.println("\n\nPlease give the User-Id that you want to delete:\n");
        long id = Long.valueOf(scanner.nextLine());
        delete(id);
        System.out.println("User deleted successfully.\n\n");
        System.out.println("The new list of users is: ");
        System.out.println(findAll());
    }

    public void findAllUsers () {
        System.out.println("\n\nList Of Users :\n");
        List<User> listOfUsers = findAll();
        if (listOfUsers.isEmpty()){
            System.out.println("There aren't any Users");
        }else {
            listOfUsers.stream().
                    forEach(System.out::println);
        }
    }


    public void findUserById () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give the User-Id:\n");
        long convertedId = Long.valueOf(scanner.nextLine());
        List<User> listOfUsers = findAll();
        List<Long> existingIds = new ArrayList<>();
        for (User user: listOfUsers) {
            long id = user.getId();
            existingIds.add(id);
        }
        if(existingIds.contains(convertedId)){
            User result = findById(convertedId);
            System.out.println("User found successfully.\n\n");
            System.out.println(result);
        } else {
            System.out.println("This Id does not exist, please enter an other Id");
            findUserById();
        }
    }

    public void findUserByEmail () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give an Email:\n");
        String userEmail = scanner.nextLine();
        List<User> listOfUsers = findAll();
        List<String> existingEmails = new ArrayList<>();
        for (User user: listOfUsers) {
            String email = user.getEmail();
            existingEmails.add(email);
        }
        if(existingEmails.contains(userEmail)){
            User result = findByEmail(userEmail);
            System.out.println("User found successfully.\n\n");
            System.out.println(result);
        } else {
            System.out.println("This Email does not exist, please enter an other Id");
            findUserByEmail();
        }
    }

    public void findAllVehiclesOfAUser () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\n Please give the User-Id :\n");
        long convertedId = Long.valueOf(scanner.nextLine());
        List<User> listOfUsers = findAll();
        List<Long> existingIds = new ArrayList<>();
        for (User user : listOfUsers) {
            long id = user.getId();
            existingIds.add(id);
        }
        if (existingIds.contains(convertedId)) {
            if (findAllVehiclesOfAUser(convertedId).isEmpty()){
                System.out.println("There are no vehicles for this user");
            } else{
                System.out.println("User Vehicles: \n\n");
                findAllVehiclesOfAUser(convertedId).forEach(System.out::println);
            }
        } else {
            System.out.println("This Id does not exist, please enter an other Id");
            findAllVehiclesOfAUser();
        }
    }


    public void findAllRepairsOfAVehicleOfASpecificUser () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\n Please give the User-Id :\n");
        long convertedId = Long.valueOf(scanner.nextLine());
        List<User> listOfUsers = findAll();
        List<Long> existingIds = new ArrayList<>();
        for (User user : listOfUsers) {
            long id = user.getId();
            existingIds.add(id);
        }
        if (existingIds.contains(convertedId)) {
            if (findAllRepairsOfAllVehiclesOfASpecificUser(convertedId).isEmpty()){
                System.out.println("There are no repairs for this user");
            } else{
                System.out.println("Repairs : \n\n");
                findAllRepairsOfAllVehiclesOfASpecificUser(convertedId).forEach(System.out::println);
            }
        } else {
            System.out.println("This Id does not exist, please enter an other Id");
            findAllVehiclesOfAUser();
        }
    }


    public User insert(long id, String email, String password, String firstName, String lastName, String address, int afm, UserType type) {
        return userService.insert(id, email, password, firstName, lastName, address, afm, type);
    }

    public boolean delete(long id){
        return userService.delete(id);
    }

    public boolean updateById(User user) {
        return userService.updateById(user);
    }

    public List<User> findAll(){
        return userService.findAll();
    }

    public User findById(long id) {
        return userService.findById(id);
    }

    public User findByEmail(String email) {
        return userService.findByEmail(email);
    }

    public boolean login(String email, String password){

        return userService.login(email, password);
    }

    public List<Vehicle> findAllVehiclesOfAUser(long userId){
        return userService.findAllVehiclesOfAUser(userId);
    }

    public List<Repair> findAllRepairsOfAllVehiclesOfASpecificUser(long userid){
        return userService.findAllRepairsOfAllVehiclesOfASpecificUser(userid);
    }










    }
