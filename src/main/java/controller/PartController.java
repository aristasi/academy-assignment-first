package controller;

import domain.Part;
import service.PartService;
import service.PartServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PartController {

    PartService partService = new PartServiceImpl();

    public String partDisplayMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("------------Part Menu------------\n");
        System.out.println("Please select a menu to navigate:\n");
        System.out.println("1) Add");
        System.out.println("2) Delete ");
        System.out.println("3) Find All ");
        System.out.println("4) Find by id ");
        System.out.println("5) Find by type ");
        System.out.println("enter 0 to return to main menu.");
        System.out.print("Option: ");
        String option = scanner.next();
        switch (option) {
            case "1": createPart();
                return option;
            case "2": deletePart();
                return option;
            case "3":findAllParts();
                return option;
            case "4": findPartById();
                return option;
            case "5": findPartByType();
                return option;
            case "0":
                return option;
            default: System.out.println("\nInvalid option, please re-enter.\n");
                return partDisplayMenu();
        }
    }

    public void createPart() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give data for the part:\n");
        System.out.println(" Give a part ID: ");
        String partId = scanner.nextLine();
        long id = Long.valueOf(partId);
        System.out.println(" Name: ");
        String name = scanner.nextLine();
        System.out.println(" Type: ");
        String type = scanner.nextLine();
        System.out.println(" Cost ");
        String cost = scanner.nextLine();
        Double costOfPart = Double.valueOf(cost);
        insert(id,name,type,costOfPart);
        System.out.println("Part entered successfully.\n\n");
        System.out.println(findById(id));
    }

    public void deletePart() {
        Scanner scanner = new Scanner(System.in);
        List<Part> listOfParts = findAll();
        System.out.println(listOfParts);
        System.out.println("\n\nPlease give the Part-Id that you want to delete:\n");
        String partId = scanner.nextLine();
        long id = Long.valueOf(partId);
        delete(id);
        Part part = findById(id);
        System.out.println("Part deleted successfully.\n\n");
        System.out.println("The new list of parts is: ");
        System.out.println(findAll());
    }

    public void findAllParts () {
        System.out.println("\n\nList Of Parts :\n");
        List<Part> listOfParts = findAll();
        if (listOfParts.isEmpty()){
            System.out.println("There aren't any Parts");
        }else {
            listOfParts.stream().
                    forEach(System.out::println);
            System.out.println("");
        }
    }

    public void findPartById () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give the Part-Id:\n");
        String partId = scanner.nextLine();
        long convertedId = Long.valueOf(partId);
        List<Part> listOfParts = findAll();
        List<Long> existingIds = new ArrayList<>();
        for (Part part: listOfParts) {
            long id = part.getId();
            existingIds.add(id);
        }
        if(existingIds.contains(convertedId)){
            Part result = findById(convertedId);
            System.out.println("Part found successfully.\n\n");
            System.out.println(result);
        } else {
            System.out.println("This Id does not exist, please enter an other Id");
            findPartById();
        }
    }

    public void findPartByType () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nPlease give a Type:\n");
        String type = scanner.nextLine();
        List<Part> listOfParts = findAll();
        List<String> existingTypes = new ArrayList<>();
        for (Part part: listOfParts) {
            String typeOfParts = part.getType();
            existingTypes.add(typeOfParts);
        }
        if(existingTypes.contains(type)){
            Part result = findByType(type);
            System.out.println("Part found successfully.\n\n");
            System.out.println(result);
        } else {
            System.out.println("This Type does not exist, please enter an other Type");
            findPartByType();
        }
    }









    public Part insert(long id, String name, String type, double cost){
        return partService.insert(id, name, type, cost);
    }

    public List<Part> findAll(){

        return partService.findAll();
    }

    public Part findById(long id){

        return partService.findById(id);
    }

    public Part findByType(String type){

        return partService.findByType(type);
    }

    public boolean delete(long id){

        return partService.delete(id);
    }







}
