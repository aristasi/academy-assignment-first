package persistence;

import domain.User;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static utilities.UserType.ADMIN;

class UserDAOImplTest {

    @Test
    void insertAUser() {
        User user = new User(1, "aris@aris.gr", "newpass", "Aris", "Tasios", "Triantafyllidi 33", 133527469, ADMIN);
        UserDAOImpl userDAO = new UserDAOImpl();
        userDAO.insert(user);
        List<User> userList = userDAO.findAll();
        assertEquals(1,userList.size());
    }

    @Test
    void delete() {
    }

    @Test
    void updateById() {
    }

    @Test
    void findAll() {
    }

    @Test
    void findById() {
    }

    @Test
    void findByEmail() {
    }

    @Test
    void login() {
    }

    @Test
    void findAllVehiclesOfAUser() {
    }
}